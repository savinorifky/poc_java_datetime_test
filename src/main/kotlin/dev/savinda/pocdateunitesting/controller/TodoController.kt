package dev.savinda.pocdateunitesting.controller

import dev.savinda.pocdateunitesting.model.AddTodoRequest
import dev.savinda.pocdateunitesting.model.AddTodoResponse
import dev.savinda.pocdateunitesting.model.WebResponse
import dev.savinda.pocdateunitesting.service.TodoService
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*

@RestController
class TodoController(
    private val todoService: TodoService
) {

    @PostMapping(
        path = ["/api/todo"],
        produces = [MediaType.APPLICATION_JSON_VALUE],
        consumes = [MediaType.APPLICATION_JSON_VALUE],
    )
    fun add(
        @RequestBody addTodoRequest: AddTodoRequest
    ): WebResponse<AddTodoResponse> {
        return WebResponse(
            todoService.add(addTodoRequest)
        )
    }

    @GetMapping(
        path = ["/api/todo"],
        produces = [MediaType.APPLICATION_JSON_VALUE],
    )fun get(
        @RequestParam(value = "tes", required = true) tes: String
    ): WebResponse<String> {
        return WebResponse(
            tes
        )
    }
}