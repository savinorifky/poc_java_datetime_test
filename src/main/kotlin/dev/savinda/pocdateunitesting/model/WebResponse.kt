package dev.savinda.pocdateunitesting.model

data class WebResponse<T>(
    val data: T
)
