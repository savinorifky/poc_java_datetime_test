package dev.savinda.pocdateunitesting.model

import java.util.*

data class AddTodoResponse(
    val todo: String? = null,

    val at: Date? = null,
)
