package dev.savinda.pocdateunitesting.model

import java.util.*

data class AddTodoRequest(
    val todo: String,

    val at: Date,
)