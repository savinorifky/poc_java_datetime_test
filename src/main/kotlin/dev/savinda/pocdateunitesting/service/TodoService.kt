package dev.savinda.pocdateunitesting.service

import dev.savinda.pocdateunitesting.model.AddTodoRequest
import dev.savinda.pocdateunitesting.model.AddTodoResponse

interface TodoService {
    fun add(request: AddTodoRequest): AddTodoResponse
}