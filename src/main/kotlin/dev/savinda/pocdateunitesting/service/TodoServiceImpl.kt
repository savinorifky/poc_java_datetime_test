package dev.savinda.pocdateunitesting.service

import dev.savinda.pocdateunitesting.model.AddTodoRequest
import dev.savinda.pocdateunitesting.model.AddTodoResponse
import org.springframework.stereotype.Service
import java.time.Clock
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId

@Service
class TodoServiceImpl constructor(
) : TodoService {
    override fun add(request: AddTodoRequest): AddTodoResponse {
        val clock = Clock.systemDefaultZone()
        val dateRequest = LocalDateTime.ofInstant(request.at.toInstant(), ZoneId.systemDefault()).toLocalDate()

        if (dateRequest.isBefore(LocalDate.now(clock))) {
            return AddTodoResponse()
        }

        return AddTodoResponse(
            todo = request.todo,
            at = request.at
        )
    }
}