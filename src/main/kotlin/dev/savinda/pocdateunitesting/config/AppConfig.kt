package dev.savinda.pocdateunitesting.config

import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component
import java.time.Clock


@Component
class AppConfig {
    @Bean
    fun clock(): Clock? {
        return Clock.systemDefaultZone()
    }
}