package dev.savinda.pocdateunitesting

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PocDateUnitestingApplication

fun main(args: Array<String>) {
    runApplication<PocDateUnitestingApplication>(*args)
}
