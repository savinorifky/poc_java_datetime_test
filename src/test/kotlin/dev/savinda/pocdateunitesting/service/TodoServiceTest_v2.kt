package dev.savinda.pocdateunitesting.service

import dev.savinda.pocdateunitesting.model.AddTodoRequest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import java.time.Clock
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.*

@ExtendWith(MockitoExtension::class)
class TodoServiceTest_v2 {

    @Mock
    private lateinit var clock: Clock

    @InjectMocks
    private lateinit var todoService: TodoServiceImpl

    @Test
    internal fun itShouldReturnTodo() {
        val date = LocalDateTime.parse("2021-09-07T00:00:00")
        with(todoService.add(AddTodoRequest(
            todo = "Test",
            at = Date.from(date.toInstant(ZoneOffset.of("+7"))),
        ))) {
            Assertions.assertEquals(todo, "Test")
        }
    }

    @Test
    internal fun itShouldReturnNull() {
        val date = LocalDateTime.parse("2021-09-06T00:00:00")
        with(todoService.add(AddTodoRequest(
            todo = "Test",
            at = Date.from(date.toInstant(ZoneOffset.of("+7"))),
        ))) {
            Assertions.assertNull(todo)
        }
    }
}