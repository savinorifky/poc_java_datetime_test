package dev.savinda.pocdateunitesting.service

import dev.savinda.pocdateunitesting.model.AddTodoRequest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import java.time.Clock
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.util.*

@ExtendWith(MockitoExtension::class)
class TodoServiceTest_v3 {

    @Mock
    private lateinit var clock: Clock

    @InjectMocks
    private lateinit var todoService: TodoServiceImpl

    private lateinit var setDate: ZonedDateTime

    @BeforeEach
    fun setUp() {
        setDate = ZonedDateTime.parse("2021-01-02T00:00:00+07:00")
//        `when`(clock.instant()).thenReturn(setDate.toInstant())
//        `when`(clock.zone).thenReturn(setDate.offset)

    }

    @Test
    internal fun itShouldReturnTodo() {
        Clock.fixed(setDate.toInstant(), setDate.offset)
        val date = LocalDateTime.parse("2021-01-02T00:00:00")
        with(todoService.add(AddTodoRequest(
            todo = "Test",
            at = Date.from(date.toInstant(ZoneOffset.of("+7"))),
        ))) {
            Assertions.assertEquals(todo, "Test")
        }
    }

    @Test
    internal fun itShouldReturnNull() {
        Clock.fixed(setDate.toInstant(), setDate.offset)
        val date = LocalDateTime.parse("2021-01-01T00:00:00")
        with(todoService.add(AddTodoRequest(
            todo = "Test",
            at = Date.from(date.toInstant(ZoneOffset.of("+7"))),
        ))) {
            Assertions.assertNull(todo)
        }
    }
}